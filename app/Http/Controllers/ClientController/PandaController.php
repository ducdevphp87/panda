<?php

namespace App\Http\Controllers\ClientController;

use App\ChamCong;
use App\UserPanda;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except' => ['salary']]);
    }

    public function salary(Request $request) {
        $year   =   $request->year?$request->year:date('Y');
        $month  =   $request->month?$request->month:date('m');
        $data['data']   =   ChamCong::groupBy('user_id')
            ->join('user', 'chamcong.user_id', '=', 'user.id')
            ->selectRaw('user.name, user_id, sum(tien_luong) as sum, sum(total_minutes) as minutes')
            ->whereMonth('ngaycheck', $month)
            ->whereYear('ngaycheck', $year)
            ->get();
        $data['sum_salary'] =   0;
        return view('panda.salary',$data);
    }

    public function salaryForEmp(Request $request) {
        $year   =   $request->year?$request->year:date('Y');
        $month  =   $request->month?$request->month:date('m');
        $user_id    =   $request->id;
        $data['data']   =   ChamCong::join('user', 'chamcong.user_id', '=', 'user.id')
            ->selectRaw( 'chamcong.id, user.name, user_id, tien_luong, total_minutes, ngaycheck, hour_in, hour_out, minutes_in, minutes_out')
            ->whereMonth('ngaycheck', $month)
            ->whereYear('ngaycheck', $year)
            ->where('user_id', $user_id)
            ->get();
        return view('panda.salary_for_emp',$data);
    }

    public function salaryForEmpPost(Request $request)
    {
        $user   =   UserPanda::find($request->user_id);
        $arr_id = $request->id;
        foreach ($arr_id as $key=>$id) {
            if(((int)$request->hour_out[$key] - (int)$request->hour_in[$key]) > 0)
            ChamCong::where('id', $id)
                ->update([
                    'hour_in' => $request->hour_in[$key],
                    'hour_out' => $request->hour_out[$key],
                    'minutes_in'    =>  $request->minutes_in[$key],
                    'minutes_out'   =>  $request->minutes_out[$key],
                    'total_minutes' =>  ($request->hour_out[$key]*60+$request->minutes_out[$key]) - ($request->hour_in[$key]*60+$request->minutes_in[$key]),
                    'tien_luong' =>  (($request->hour_out[$key]*60+$request->minutes_out[$key]) - ($request->hour_in[$key]*60+$request->minutes_in[$key]))/60*$user->salary
                ]);
        }
        return Redirect::back()->with('message', 'Cập nhật thành công !');

    }

    public function getAddUser() {
        return view('panda.add');
    }

    public function postAddUser(Request $request) {
        UserPanda::create($request->all());

        return redirect('/')->with('message', 'Cập nhật thành công !');
    }

    public function edit($id) {
        $data['data']   =   UserPanda::with('chamcong')->orderBy('slug','ASC')->find($id);

        return view('panda.edit', $data);;
    }

    public function update($id, Request $request) {
        $panda = UserPanda::findOrFail($id);

        $input = $request->all();

        $a = $panda->fill($input)->save();
        return redirect('/employer')->with('message', 'Cập nhật thành công !');
    }

    public function pandaEmp() {
        $data['data']   =   UserPanda::with('chamcong')->orderBy('active','DESC')->orderBy('slug','ASC')->get();

        return view('panda.employes', $data);
    }

    public function suaCong(Request $request) {
        $data['data']   =   UserPanda::with('chamcong')->orderBy('slug','ASC')->where('active',1)->get();
        return view('panda.suacong', $data);

    }

    public function suaCongPost(Request $request) {
        $chamcong       =   ChamCong::where('user_id',$request->user_id)
            ->whereDate('ngaycheck', $request->date)
            ->where('shift', $request->shift)
            ->first();
        if(!$chamcong){
            $chamcong   =   new \stdClass();
            $chamcong->check_in     =   $request->date;
            $chamcong->check_out    =   $request->date;
            $chamcong->user_id      =   $request->user_id;

        }
        $data['data']   =   $chamcong;
        return view('panda.ngay_suacong', $data);
    }
    public function ngaysuaCongPost(Request $request) {
        if($request->id>0){
            ChamCong::where('id', $request->id)
                ->where('shift', $request->shift)
                ->update(['check_in' => $request->check_in,
                    'check_out' => $request->check_out]);
        }else{
            $chamcong = new ChamCong;

            $chamcong->check_in = $request->check_in;
            $chamcong->check_out = $request->check_out;
            $chamcong->shift = $request->shift;
            $chamcong->user_id = $request->user_id;

            $chamcong->save();
        }


        return redirect('/')->with('message', 'Cập nhật thành công !');
    }

    public function suaCongByDayGet(Request $request) {
        $date   =   $request->date;
        $shift  =   $request->shift;
        $data['dataed']   =   ChamCong::join('user', 'chamcong.user_id', '=', 'user.id')
            ->selectRaw( 'chamcong.id, user.name, user_id, tien_luong, total_minutes, ngaycheck, hour_in, hour_out, minutes_in, minutes_out')
            ->whereDate('ngaycheck', $date)
            ->where('shift', $shift)
            ->get();
        $arr_user_id    =   [];
        foreach ($data['dataed'] as $user_id ) {
            $arr_user_id[]    =   $user_id['user_id'];

        }
        $data['data']   =   UserPanda::orderBy('slug','ASC')->whereNotIn('id', $arr_user_id)->where('active',1)->get();
        $data['shift']  =   $shift;
        $data['date']   =   $date;
        return view('panda.suacongbyday', $data);
    }
    public function suaCongByDayPost(Request $request) {
        foreach($request->user_id as $key=>$value){
            if($request->hour_out[$key] - $request->hour_in[$key]>0){
                $data = ChamCong::where('user_id', $value)
                    ->whereDate('ngaycheck', $request->ngaycheck)
                    ->where('shift', $request->shift)
                    ->first();
                $user   =   UserPanda::find($value);
                if ($request->hour_out[$key] - $request->hour_in[$key] > 1) {
                    if ($data) {
                        $data->update([
                            'hour_in'       =>  $request->hour_in[$key],
                            'minutes_in'    =>  $request->minutes_in[$key],
                            'hour_out'      =>  $request->hour_out[$key],
                            'minutes_out'   =>  $request->minutes_out[$key],
                            'total_minutes' =>  ($request->hour_out[$key]*60+$request->minutes_out[$key]) - ($request->hour_in[$key]*60+$request->minutes_in[$key]),
                            'tien_luong' =>  (($request->hour_out[$key]*60+$request->minutes_out[$key]) - ($request->hour_in[$key]*60+$request->minutes_in[$key]))/60*$user->salary

                        ]);
                    }else {
                        DB::table('chamcong')->insert(
                            ['user_id' => $value,
                                'ngaycheck' => $request->ngaycheck,
                                'hour_in'       =>  $request->hour_in[$key],
                                'minutes_in'    =>  $request->minutes_in[$key],
                                'hour_out'      =>  $request->hour_out[$key],
                                'minutes_out'   =>  $request->minutes_out[$key],
                                'shift'         =>  $request->shift,
                                'total_minutes' =>  ($request->hour_out[$key]*60+$request->minutes_out[$key]) - ($request->hour_in[$key]*60+$request->minutes_in[$key]),
                                'tien_luong' =>  (($request->hour_out[$key]*60+$request->minutes_out[$key]) - ($request->hour_in[$key]*60+$request->minutes_in[$key]))/60*$user->salary
                            ]
                        );
                    }
                }

            }

        }
        return redirect('/')->with('message', 'Cập nhật thành công !');    }
}
