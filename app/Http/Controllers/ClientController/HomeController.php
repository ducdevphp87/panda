<?php

namespace App\Http\Controllers\ClientController;

use App\UserPanda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ChamCong;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index(){
        $data['data']   =   UserPanda::with('chamcong')->orderBy('slug','ASC')->where('active',1)->get();

        return view('panda.index', $data);
    }
    public function checkin(Request $request){
        $user_id        =   $request->user_id;
        $hour           =   date('Y-m-d H:i:s');
        $shift  =   date('H')<16?0:1;
        $data   =   ChamCong::where('user_id', $user_id)->whereDate('ngaycheck', Carbon::today())
            ->where('shift', $shift)->first();
        if (!$data) {
            $chamcong = new ChamCong();

            $chamcong->user_id      =   $user_id;
            $chamcong->check_in     =   $hour;
            $chamcong->ngaycheck    =   $hour;
            $chamcong->hour_in      =   date('H');
            $chamcong->minutes_in   =   date('i');
            $chamcong->shift        =   $shift;

            $chamcong->save();
        }

    }
    public function checkout(Request $request){
        $user_id        =   $request->user_id;
        $shift          =   $request->shift;
        $hour           =   date('Y-m-d H:i:s');
        $data   =   ChamCong::where('user_id', $user_id)->whereDate('ngaycheck', Carbon::today())
            ->where('shift', $shift)->first();
        $total_minutes  =   (date('H') * 60 +  date('i')) - ($data['hour_in']*60 + $data['minutes_in']);
        $data_user      =   UserPanda::find($user_id);
        $tien_luong     =   $total_minutes / 60 * $data_user->salary;
        $data->update(['check_out'=>$hour,
                        'hour_out'=>date('H'),
                        'total_minutes'=>$total_minutes,
                        'tien_luong'=>$tien_luong,
                        'minutes_out'=>date('i')]);

    }
    public function doLogout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::to('login'); // redirect the user to the login screen
    }
}
