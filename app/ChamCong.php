<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChamCong extends Model
{
    protected $table = 'chamcong';
    protected $fillable = ['check_in','check_out','ngaycheck','shift','hour_in','minutes_in','hour_out','minutes_out','user_id','total_minutes','tien_luong','total_cong'];
    public function userPanda()
    {
        return $this->belongsTo('App\UserPanda','user_id','id')->where('active',1);
    }
}
