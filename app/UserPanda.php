<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class UserPanda extends Model
{
    protected $table = 'user';
    protected $fillable = ['name','alias','slug','cmt','phone','active','join','salary'];
    public function chamcong(){
        $shift  =   date('H')<16?0:1;

        return $this->hasMany('App\ChamCong','user_id','id')->where('shift', $shift)->whereDate('ngaycheck', Carbon::today());
    }


    public function chamcongtab(){

        return $this->hasMany('App\ChamCong','user_id','id');
    }

}
