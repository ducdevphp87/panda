<div class="modal fade mymodal" id="md_hotline">
    <div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="center">
                <h4 class="modal-title">Hotline</h4>
                <span>8:30 - 21:30</span>
            </div>
		</div>
        <div class="modal-body">
			<div class="hotline-group">
                <div class="flex-box item">
                    <span class="title">Mua hàng & CSKH</span>
                    <div class="line-gr flex-box">
                        <span class="line-num">1080 9088</span>
                        <span><i class="fa fa-phone"></i></span>
                    </div>
                </div>
                <div class="flex-box item">
                    <span class="title">Mua hàng & CSKH</span>
                    <div class="line-gr flex-box">
                        <span class="line-num">1080 9088</span>
                        <span><i class="fa fa-phone"></i></span>
                    </div>
                </div>
            </div>
		</div>
    </div>
</div>
