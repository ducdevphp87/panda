<header id="header" class="">
    <section class="top-header bg-red relative">
        <div class="container">
            <button id="btn_toggle"><i class="icon-bars"></i></button>
            <a href="/" title="dibao.com.vn" class="logo"><img src="pictures/logo.png" alt="dibao.com.vn" /></a>
            <a href="/" title="dibao.com.vn" class="logo-r logo"><img src="pictures/logo-mobile.png" alt="dibao.com.vn" /></a>
            <div class="header-right flex-box hidden-xs">
                <ul class="help-link">
                    <li><a href="/lien-he.html" title="Liên hệ">Liên hệ</a></li>
                    <li><a href="javascript:;" title="Câu hỏi thường gặp">Câu hỏi thường gặp</a></li>
                </ul>
                <ul class="social right">
                    <li><a href="javascript:;"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="javascript:;"><i class="fab fa-linkedin"></i></a></li>
                    <li><a href="javascript:;"><i class="fab fa-instagram"></i></a></li>
                    <li><a href="javascript:;"><i class="fab fa-youtube"></i></a></li>
                </ul>
            </div>
            <button id="btn_tog_m"><span class="icon-find-o"></span></button>
        </div>
    </section>
    <div id="in_search_m">
        <div class="container">
            <div class="search-inner">
                <form action="/ket-qua-tim-kiem.html" method="POST">
                    <input type="text" name="keyword" value="Tìm kiếm...">
                    <button><span class="fa fa-search"></span></button>
                </form>
            </div>
        </div>
    </div>
    <section class="menu-bar">
        <div class="container">
            <div id="menubar">
                <ul class="menu">
                    <li class="active"><a href="/" title="Dibao.com.vn">Trang chủ</a></li>
                    <li><a href="/ve-dibao-n37" title="Về Dibao">Về Dibao</a></li>
                    <li class="dropdown2"><a href="/xe-may-dien" title="Xe máy điện">Xe máy điện <i class="fa fa-chevron-down"></i></a>
                        <div class="sub2">
                            <ul class="row">
                                @for($i=0;$i<6;$i++)
                                <li class="col-md-2 col-xs-6">
                                    <a href="javascript:;" title="">
                                        <img src="pictures/zoomer-dibao-2016.png" alt="">
                                        <span>Xe điện Goloo Dibao</span>
                                    </a>
                                </li>
                                @endfor
                            </ul>
                        </div>
                    </li>
                    <li><a href="/xe-dap-dien" title="Xe đạp điện">Xe 50CC <i class="fa fa-chevron-down"></i></a></li>
                    <li><a href="/daily.html" title="Xe đạp điện">Hệ thống Đại lý <i class="fa fa-chevron-down"></i></a></li>
                    <li><a href="/bao-hanh-n32" title="Bảo hành">Bảo hành</a></li>
                    <li><a href="/van-chuyen-n19" title="Vận chuyển">Vận chuyển</a></li>
                    <li><a href="/tin-tuc-n24" title="Tin tức">Tin tức <i class="fa fa-chevron-down"></i></a>
                        <ul class="sub">
                            <li><a href="javascript:;" title=""><i class="fa fa-chevron-right"></i>Xe điện Goloo Dibao</a></li>
                            <li><a href="javascript:;" title=""><i class="fa fa-chevron-right"></i>Xe điện Goloo Dibao</a></li>
                        </ul>
                    </li>
                    <li class="search-box hidden-xs">
                        <button class="btn-tog" onclick="togglesearch(this)"><span class="icon-find"></span></button>
                        <div class="search-inner">
                            <form action="/ket-qua-tim-kiem.html" method="POST">
                                <input type="text" name="keyword" value="Tìm kiếm...">
                                <button><span class="fa fa-search"></span></button>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
</header>
