<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome550/css/all.min.css')}}">
    @yield('style')
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="row">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/" >
                Trang chủ
            </a>
        </li>
        <li class="list-group-item">
            <a href="/salary" >
                Lương tháng
            </a>
        </li>
        <li class="list-group-item">
            <a href="/addUser" >
                Thêm mới nhân viên
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ action('ClientController\PandaController@pandaEmp') }}" >
                Danh sách nhân viên
            </a>
        </li>
        {{--<li class="list-group-item">--}}
            {{--<a href="{{ action('ClientController\PandaController@suaCong') }}" >--}}
                {{--Sửa công theo nhân viên--}}
            {{--</a>--}}
        {{--</li>--}}
        <li class="list-group-item">
            <a href="/select_day" >
                Sửa công theo ngày
            </a>
        </li>
        <li class="list-group-item">
            <h1>@if(date('H')<17) Ca trưa @else Ca tối @endif </h1>
        </li>
    </ul>
</div>
<div>
</div>
<div class="row">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
</div>
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Tên</th>
        <th scope="col">Số điện thoại</th>
        <th scope="col">Ngày bắt đầu</th>
        <th scope="col">Cmt</th>
        <th scope="col">Đang làm việc</th>

    </tr>
    </thead>
    <tbody>
    @foreach($data as $key => $item)

        <tr>
            <th scope="row">
                <a href="{{ action('ClientController\PandaController@edit', ['id' => $item->id]) }}" >
                    {{$key+1}}
                </a>
            </th>
            <td>
                <a href="{{ action('ClientController\PandaController@edit', ['id' => $item->id]) }}" >
                    {{$item->name}}
                </a>
            </td>
            <td>
                <a href="#" >
                    {{$item->phone}}
                </a>
            </td>
            <td>
                <a href="#" >
                    {{date('Y-m-d',strtotime($item->join))}}
                </a>
            </td>
            <td>
                <a href="#" >
                    {{$item->cmt}}
                </a>
            </td>
            <td>
                @if($item->active==1)Làm việc @else Nghỉ việc @endif
            </td>
        </tr>
    @endforeach
    </tbody>

</table>
</body>
</html>

