<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome550/css/all.min.css')}}">
    @yield('style')
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body >
<div class="row">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/" >
                Trang chủ
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ action('ClientController\PandaController@pandaEmp') }}" >
                Danh sách nhân viên
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ action('ClientController\PandaController@suaCong') }}" >
                Sửa công theo nhân viên
            </a>
        </li>
        <li class="list-group-item">
            <a href="/select_day" >
                Sửa công theo ngày
            </a>
        </li>
        <li class="list-group-item">
            <h1>{{$date}} </h1>
        </li>
    </ul>
</div>
<div>
</div>
<div class="row">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
</div>
<form method="POST" action="{{ route('sua_cong_post')}}">
    {{csrf_field()}}
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Tên</th>
            <th scope="col">Hour_in</th>
            <th scope="col">Minutes_in</th>
            <th scope="col">Hour_out</th>
            <th scope="col">Minutes_out</th>
        </tr>
        </thead>
        <tbody>
        @if($dataed)
            @foreach($dataed as $itemed)
                <tr>
                    <td scope="row">
                        <a href="{{ action('ClientController\PandaController@suaCongByDayPost', ['id' => $itemed->id]) }}" >
                            {{$itemed->user_id}}
                        </a>
                    </td>
                    <td>
                        <a href="{{ action('ClientController\PandaController@suaCongByDayPost', ['id' => $itemed->id]) }}" >
                            {{$itemed->name}}
                        </a>
                    </td>
                    <td>
                        <select name="hour_in[]">
                            @if($shift==0)
                                @for($i=10;$i<=15;$i++)
                                    <option value="{{$i}}" @if($itemed->hour_in==$i) selected @endif>{{$i}}</option>
                                @endfor
                            @else
                                @for($i=17;$i<=23;$i++)
                                    <option value="{{$i}}" @if($itemed->hour_in==$i) selected @endif>{{$i}}</option>
                                @endfor
                            @endif
                        </select>
                    </td>
                    <td>
                        <select name="minutes_in[]">
                            @for($i=0;$i<=60;$i++)
                                <option value="{{$i}}" @if($itemed->minutes_in==$i) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </td>
                    <td>
                        <select name="hour_out[]">
                            @if($shift==0)
                                @for($i=10;$i<=17;$i++)
                                    <option value="{{$i}}" @if($itemed->hour_out==$i) selected @endif>{{$i}}</option>
                                @endfor
                            @else
                                @for($i=17;$i<24;$i++)
                                    <option value="{{$i}}" @if($itemed->hour_out==$i) selected @endif>{{$i}}</option>
                                @endfor
                            @endif
                        </select>
                    </td>
                    <td>
                        <select name="minutes_out[]">
                            @for($i=0;$i<=60;$i++)
                                <option value="{{$i}}" @if($itemed->minutes_out==$i) selected @endif>{{$i}}</option>
                            @endfor
                        </select>
                    </td>
                </tr>
                <input type="hidden" name="user_id[]" value="{{$itemed->user_id}}">
            @endforeach
        @endif
        @foreach($data as $key => $item)

            <tr>
                <td scope="row">
                    <a href="{{ action('ClientController\PandaController@suaCongByDayPost', ['id' => $item->id]) }}" >
                        {{$item->id}}
                    </a>
                </td>
                <td>
                    <a href="{{ action('ClientController\PandaController@suaCongByDayPost', ['id' => $item->id]) }}" >
                        {{$item->name}}
                    </a>
                </td>
                <td>
                    <select name="hour_in[]">
                        @if($shift==0)
                            @for($i=10;$i<=13;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        @else
                            @for($i=17;$i<=20;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        @endif
                    </select>
                </td>
                <td>
                    <select name="minutes_in[]">
                        @for($i=0;$i<=60;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </td>
                <td>
                    <select name="hour_out[]">
                        @if($shift==0)
                            @for($i=10;$i<=17;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        @else
                            @for($i=17;$i<24;$i++)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        @endif
                    </select>
                </td>
                <td>
                    <select name="minutes_out[]">
                        @for($i=0;$i<=60;$i++)
                            <option value="{{$i}}">{{$i}}</option>
                        @endfor
                    </select>
                </td>
            </tr>
            <input type="hidden" name="user_id[]" value="{{$item->id}}">
        @endforeach
        </tbody>

    </table>
    <input type="hidden" name="ngaycheck" value="{{$date}}">
    <input type="hidden" name="shift" value="{{$shift}}">
    <input type="submit" class="btn btn-info" value="Submit Button">

</form>
</body>
</html>

