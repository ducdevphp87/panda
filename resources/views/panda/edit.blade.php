<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="container">
    <form method="POST" action="{{ action('ClientController\PandaController@update' , ['id' => $data->id]) }}">
        <input type="hidden" name="_method" value="put" />
        {{csrf_field()}}
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input type="text" class="form-control" id="exampleInputEmail1" name="name" value="{{$data->name}}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Phone</label>
            <input type="text" class="form-control" name="phone" value="{{$data->phone}}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Chứng minh thư</label>
            <input type="text" class="form-control" name="cmt" value="{{$data->cmt}}">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Ngày bắt đầu làm việc</label>
            <input type="date" class="form-control" name="join" value="{{date('Y-m-d', strtotime($data->join))}}">
        </div>
        <div class="form-group">
            <label for="year" class="control-label input-group" >Đang làm việc</label>
            <div class="btn-group" data-toggle="buttons">
                <label>
                    <input type="radio" name="active" @if($data->active==1) checked @endif value="1">Có
                </label>
                <label>
                    <input type="radio" name="active" @if($data->active==0) checked @endif value="0">Không
                </label>
            </div>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Hệ số lương</label>
            <input type="number" class="form-control" name="salary" value="{{$data->salary}}">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
</body>
</html>