<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="container">
    <form method="POST" action="">
        {{csrf_field()}}
    <div class="form-group">
        <label for="sel1">Chọn nhân viên:</label>
        <select name="user_id" class="form-control" id="sel1">
            @foreach($data as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
    </div>
    <!-- Search form -->
    <div class="form-group">
        <label for="sel1">Chọn ngày:</label>
        <div class='input-group' id=''>
            <input type='date' name="date" class="form-control" value="{{date('Y-m-d')}}"/>
        </div>
    </div>
    <div class="form-group">
        <label for="sel1">Chọn ca:</label>
        <select name="shift" class="form-control" id="sel1">
            <option value="0">Ca trưa</option>
            <option value="1">Ca tối</option>
        </select>
    </div>
    <input type="submit" class="btn btn-info" value="Submit Button">
    </form>

</div>
</body>

</html>