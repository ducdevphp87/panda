<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome550/css/all.min.css')}}">
    @yield('style')
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="row">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/" >
                Trang chủ
            </a>
        </li>
        <li class="list-group-item">
            <a href="/salary" >
                Lương tháng
            </a>
        </li>
        <li class="list-group-item">
            <a href="/addUser" >
                Thêm mới nhân viên
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ action('ClientController\PandaController@pandaEmp') }}" >
                Danh sách nhân viên
            </a>
        </li>
        {{--<li class="list-group-item">--}}
            {{--<a href="{{ action('ClientController\PandaController@suaCong') }}" >--}}
                {{--Sửa công theo nhân viên--}}
            {{--</a>--}}
        {{--</li>--}}
        <li class="list-group-item">
            <a href="/select_day" >
                Sửa công theo ngày
            </a>
        </li>
        <li class="list-group-item">
            <h1>@if(date('H')<17) Ca trưa @else Ca tối @endif </h1>
        </li>
    </ul>
</div>
<div>
</div>
<form method="get">
    <h4>Chọn lương tháng</h4>
    <div class="form-group">
        <label for="sel1">Select list:</label>
        <select class="form-control" id="sel1" name="month">
            @for($i=1;$i<=12;$i++)
            <option>{{$i}}</option>
            @endfor
        </select>
    </div>
    <div class="form-group">
        <label for="sel1">Năm:</label>
        <select class="form-control" id="sel1" name="year">
            <option>2019</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<div class="row">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
</div>
<form method="POST" action="{{ route('salary_for_emp')}}">
    {{csrf_field()}}
<table class="table">
    <tr>
        <th>Id</th>
        <th>Ngày</th>
        <th>Phút</th>
        <th>Lương ngày</th>
        <th>Giờ vào</th>
        <th>Phút vào</th>
        <th>Giờ out</th>
        <th>Phút out</th>
    </tr>
    <tbody>
        @foreach($data as $key=>$value)
            @if($key==0)
            <h2>{{$value->name}}</h2>
            @endif
            <tr>
                <td>{{$value->user_id}}</td>
                <td>{{date('Y-m-d', strtotime($value->ngaycheck))}}</td>
                <td>{{$value->total_minutes}}</td>
                <td>{{number_format($value->tien_luong)}}.000</td>
                <td>
                    <select name="hour_in[]">
                        @for($i=9;$i<=23;$i++)
                        <option @if($i==$value->hour_in) selected @endif>{{$i}}</option>
                        @endfor
                    </select>
                </td>
                <td>
                    <select name="minutes_in[]">
                        @for($i=0;$i<=60;$i++)
                            <option @if($i==$value->minutes_in) selected @endif>{{$i}}</option>
                        @endfor
                    </select>
                </td>
                <td>
                    <select name="hour_out[]">
                        @for($i=9;$i<=23;$i++)
                            <option @if($i==$value->hour_out) selected @endif>{{$i}}</option>
                        @endfor
                    </select>
                </td>
                <td>
                    <select name="minutes_out[]">
                        @for($i=0;$i<=60;$i++)
                            <option @if($i==$value->minutes_out) selected @endif>{{$i}}</option>
                        @endfor
                    </select>
                </td>
            </tr>
            <input type="hidden" name="id[]" value="{{$value->id}}"/>
            <input type="hidden" name="user_id" value="{{$value->user_id}}"/>

        @endforeach
    </tbody>
</table>
<input type="submit" class="btn btn-info" value="Submit Button">

</form>
</body>
</html>

