<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="container">
    <form method="POST" action="/ngay_sua_cong">

    {{csrf_field()}}
        <!-- Search form -->
        <div class="form-group">
            <label for="sel1">Check in:</label>
            <div class='input-group' id=''>
                <input type="datetime" name="check_in" class="form-control"
                       value="@if(isset($data->check_in)){{date('Y-m-d H:i:s', strtotime($data->check_in))}}@endif"/>
            </div>
        </div>
        <div class="form-group">
            <label for="sel1">Check out:</label>
            <div class='input-group' id=''>
                <input type="datetime" name="check_out" class="form-control"
                       value="@if(isset($data->check_in)){{date('Y-m-d H:i:s', strtotime($data->check_out))}}@endif"/>
            </div>
        </div>
        <input type="hidden" name="id" value="{{isset($data->id)?$data->id:0}}">
        <input type="hidden" name="shift" value="{{isset($data->shift)?$data->shift:0}}">
        <input type="hidden" name="user_id" value="{{isset($data->user_id)?$data->user_id:0}}">

        <input type="submit" class="btn btn-info" value="Submit Button">
    </form>

</div>
</body>

</html>