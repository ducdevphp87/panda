<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Dibao</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="container">
    <div class="row">
        <ul class="list-group">
            <li class="list-group-item">
                <a href="/" >
                    Trang chủ
                </a>
            </li>
            <li class="list-group-item">
                <a href="/salary" >
                    Lương tháng
                </a>
            </li>
            <li class="list-group-item">
                <a href="/addUser" >
                    Thêm mới nhân viên
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ action('ClientController\PandaController@pandaEmp') }}" >
                    Danh sách nhân viên
                </a>
            </li>
            <li class="list-group-item">
                <a href="{{ action('ClientController\PandaController@suaCong') }}" >
                    Sửa công theo nhân viên
                </a>
            </li>
            <li class="list-group-item">
                <a href="/select_day" >
                    Sửa công theo ngày
                </a>
            </li>
        </ul>
    </div>
    <form method="GET" action="/sua_cong_by_day">

        {{csrf_field()}}
        <div class="form-group">
            <label for="sel1">Check out:</label>
            <div class='input-group' id=''>
                <input type="date" name="date" class="form-control" required/>
            </div>
        </div>
        <div class="form-group">
            <label for="sel1">Ca</label>
            <div class='input-group' id=''>
                <select name="shift">
                    <option value="0">Trưa</option>
                    <option value="1">Tối</option>
                </select>
            </div>
        </div>
        <input type="submit" class="btn btn-info" value="Submit Button">

    </form>

</div>
</body>

</html>