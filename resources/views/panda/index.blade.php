<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="Xe điện Dibao Việt Nam">
    <meta name="page-topic" content="Xe điện Dibao Việt Nam">
    <meta name="abstract" content="">
    <meta name="description" content="">
    <meta name="google-site-verification"content="1f2QTu5_EnLBUZj1tYli2N4kSRiWEZuYaHGee02ejsY" />
    <title>Panda</title>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.min.css')}}">
    <link rel="stylesheet" href="{{asset('fontawesome550/css/all.min.css')}}">
    @yield('style')
    <script type="text/javascript" src="{{asset('js/jquery-1.10.2.min.js')}}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">

</head>
<body>
<div class="row">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="/home" >
                Quản lý
            </a>
        </li>
        <li class="list-group-item">
            <a href="/" >
                Trang chủ
            </a>
        </li>
        <li class="list-group-item">
            <a href="/salary" >
                Lương tháng
            </a>
        </li>
        <li class="list-group-item">
            <a href="/addUser" >
                Thêm mới nhân viên
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ action('ClientController\PandaController@pandaEmp') }}" >
                Danh sách nhân viên
            </a>
        </li>
        {{--<li class="list-group-item">--}}
            {{--<a href="{{ action('ClientController\PandaController@suaCong') }}" >--}}
                {{--Sửa công theo nhân viên--}}
            {{--</a>--}}
        {{--</li>--}}
        <li class="list-group-item">
            <a href="/select_day" >
                Sửa công theo ngày
            </a>
        </li>
        <li class="list-group-item">
            <h1>@if((date('H')<17) || (date('H')==16 && date('i')<=30))  Ca trưa @else Ca tối @endif </h1>
        </li>
    </ul>
</div>
<div>
</div>
<div class="row">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
</div>
<table class="table">
    <thead>
    <tr>
        <th scope="col">#</th>
        <th scope="col">Tên</th>
        <th scope="col">Checkin</th>
        <th scope="col">Checkout</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $key => $item)

    <tr>
        <th scope="row">
            <a href="{{ action('ClientController\PandaController@edit', ['id' => $item->id]) }}" >
                {{$key+1}}
            </a>
        </th>
        <td>
            <a href="{{ action('ClientController\PandaController@edit', ['id' => $item->id]) }}" >
                {{$item->name}}

            </a>
        </td>
        <td>
            <button @if(count($item->chamcong)>0) disabled @endif type="button" class="btn btn-primary checkin_{{$item->id}}" onclick="checkin({{$item->id}})" name="checkin" >CheckIn</button>
        </td>
        <td>
            <button @if(count($item->chamcong)>0 && $item->chamcong[0]->hour_out == 0) @else disabled @endif type="button" class="btn btn-danger checkout_{{$item->id}}" onclick="checkout({{$item->id}})" name="checkout" >CheckOut</button>
        </td>
    </tr>
        @endforeach
    </tbody>

</table>
<input type="hidden" name="shift" value="@if(date('H')<17) {{0}} @else {{1}} @endif" />
<script>
    function checkin(user_id){
        if(confirm ('Are you sure?')){
            var CSRF_TOKEN  =   $('meta[name="csrf-token"]').attr('content');
            var shift       =   $('input[name=shift]').val();
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url:"/checkin",
                type:"POST",
                data:{
                    "_token": "{{ csrf_token() }}",
                    'user_id':user_id
                },
                success:function(data) {
                    $('.checkin_'+user_id).attr("disabled", true);
                    $('.checkout_'+user_id).attr("disabled", false);

                }
            });
        }

    }
    function checkout(user_id){
        if (confirm('Are you sure?')){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var shift       =   $('input[name=shift]').val();
            $.ajax({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url:"/checkout",
                type:"POST",
                data:{
                    "_token":   "{{ csrf_token() }}",
                    'user_id':  user_id,
                    "shift" :   shift
                },
                success:function(data) {
                    $('.checkout_'+user_id).attr("disabled", true);
                }
            });
        }

    }
</script>
</body>
</html>

