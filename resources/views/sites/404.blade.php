@extends('layouts.app')

@section('content')
    <section class="module">
        <div class="container">
            <div class="notfound">
                <img src="/pictures/404.png" alt="">
                <h4>OOPS, SORRY WE CAN’T FIND THAT PAGE !</h4>
                <p>The page you are looking for no longer exists. Perhaps you can return back to the site’s homepage and see if you can find what you are looking for.</p>

                <a href="/" class="btn btn-submit">Back to home</a>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
