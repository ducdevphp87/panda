@extends('layouts.app')

@section('content')
    <section class="padding-40t margin-30b">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-2 contact-form">
                    <div class="box">
                        <h3>Contact us</h3>
                        <form class="form">
                            <div class="form-group">
                                <div class="box">
                                    <label class="col-md-4">Full name *</label>
                                    <div class="col-md-8">
                                        <input type="text" name="name" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="box">
                                    <label class="col-md-4">Email *</label>
                                    <div class="col-md-8">
                                        <input type="text" name="email" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="box">
                                    <label class="col-md-4">Phone number *</label>
                                    <div class="col-md-8">
                                        <input type="text" name="phone" value="" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="box">
                                    <label class="col-md-4">Message *</label>
                                    <div class="col-md-8">
                                        <textarea name="message" rows="8" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="box">
                                    <label class="col-md-4">Verification *</label>
                                    <div class="col-md-8">
                                        <input type="text" name="captcha" value="" class="col-xs-8 captcha">
                                        <div class="col-xs-4">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group center">
                                <button class="btn btn-submit">send</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
