@extends('layouts.app')

@section('content')
    <section class="module" style="padding-top:40px">
        <div class="container">
            <div class="col-md-8 col-md-offset-2 about">
                <h2>About us / Term and Conditions / Privacy Policies</h2>

                <div class="box margin-10b">
                    <strong>Benefits for the Buyers</strong>

                    <p>là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                </div>
                <div class="box margin-10b">
                    <strong>Benefits for the Buyers</strong>
                    <p>là nền tảng công nghệ thông tin (CNTT) kết nối nhu cầu thuê dịch vụ lưu trú du lịch giữa: Bên cung cấp dịch vụ lưu trú du lịch & Bên có nhu cầu sử dụng dịch vụ lưu trú du lịch. Theo pháp luật địa phương nơi hoạt động, Luxstay cam kết không cung cấp dịch vụ cho thuê nhà/dịch vụ lưu trú du lịch, mà chỉ hoạt động như một trung gian kết nối và cung cấp tiện ích CNTT cho các bên tham gia trên hệ thống Luxstay.</p>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
