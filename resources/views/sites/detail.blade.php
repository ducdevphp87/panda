@extends('layouts.app')

@section('content')
    <section class="top-detail">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="carousel slide" id="carousel_thumbnail" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="item active" data-slide-number="1">
                                <img src="pictures/xe-dien-vespas-dibao@2x.png" alt="...">
                            </div>
                            <div class="item" data-slide-number="2">
                                <img src="pictures/Sur_Ron-8__44179.1513042993@2x.png" alt="...">
                            </div>
                            <div class="item" data-slide-number="3">
                                <img src="pictures/zoomer-dibao-2016@2x.png" alt="...">
                            </div>
                        </div>
                    </div>
                    <div id="nav_thumb">
                        <ul class="list-inline flex-box">
                            @for($i=1;$i<4;$i++)
                            <li>
                                <a href="javascript:;" id="carousel-selector-{{$i}}" class="">
                                    <img src="pictures/xe-dien-vespas-dibao.png" alt="">
                                </a>
                            </li>
                            @endfor
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="info-box center">
                        <h1>Xe điện Goloolo Dibao</h1>
                        <p>Trải nghiệm công nghệ và sắc màu</p>
                        <span class="price">15.000.000đ</span>
                        <div class="margin-30b flex-box">
                            <span>Màu sắc</span> : &nbsp;<ul class="list-color">
                                <li><i></i></li>
                                <li><i></i></li>
                                <li><i></i></li>
                                <li><i></i></li>
                            </ul>
                        </div>
                        <a href="/daily.html" title="Tìm đại lý gần bạn" class="link-detail" target="_blank">Tìm đại lý gần bạn <span class="fa fa-chevron-right"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="heading">
                <h3>MÔ TẢ CHI TIẾT</h3>
            </div>
            <article class="detail-content">

            </article>
            <div class="heading">
                <h3>VIDEO</h3>
            </div>
            <article class="detail-content">
                <img src="pictures/xe-dien-gopro-dibao_00013@2x.png" alt="">
            </article>
            <div class="heading">
                <h3>Thông số kỹ thuật</h3>
            </div>
            <article class="detail-content">
                <img src="pictures/Group 1654@2x.png" alt="">
            </article>
        </div>
    </section>
    <section class="module">
        <div class="container">
            <div class="heading">
                <h3>Sản phẩm cùng loại</h3>
            </div>
            <div class="row">
                <ul class="list list-product">
                    @for($i=0;$i<6;$i++)
                    <li class="col-md-4">
                        <div class="inner">
                            <div class="thumb">
                                <a href="/xe-dien-vespas-dibao.html" title=""><img src="pictures/xe-dien-vespas-dibao@2x.png" alt="" /></a>
                                <div class="shadow-info">
                                    <strong>Đặc điểm nổi bật</strong>
                                    <p>phanh đĩa trước</p>
                                    <p>Xe điện Dibao Nami mang một thiết kế hoàn toàn mới nhỏ gọn, thanh thoát điều này thể hiện rõ nét qua mặt trước của xe</p>
                                    <p>Đồng hồ điện tử</p>
                                </div>
                            </div>
                            <div class="entry">
                                <a href="/xe-dien-vespas-dibao.html" title="Xe điện Dibao" class="title">Xe điện Dibao</a>
                                <div class="pr">
                                    Giá bán từ : <span class="price">15.000.000đ</span>
                                </div>
                                <a href="/xe-dien-vespas-dibao.html" title="Chi tiết" class="link-detail">Xem chi tiết <span class="fa fa-chevron-right"></span></a>
                            </div>
                        </div>
                    </li>
                    @endfor
                </ul>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="" id="comment_box">

            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
@section('style')

@stop
@section('script')
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>

<script type="text/javascript">
    $('#carousel_thumbnail').carousel({interval:false});

    $('[id^=carousel-selector-]').click(function() {
        var id_selector = $(this).attr("id");
        var id = id_selector.substr(id_selector.length - 1);
        //console.log(id);
        id = parseInt(id);
        $('#carousel_thumbnail').carousel(id - 1);
        $('[id^=carousel-selector-]').removeClass('selected');
        $(this).addClass('selected');
    });
    $('#carousel_thumbnail').on('slid.bs.carousel', function(e) {
        var id = $('.item.active').data('slide-number');
        id = parseInt(id);
        $('[id^=carousel-selector-]').removeClass('selected');
        $('[id=carousel-selector-' + id + ']').addClass('selected');
    });
</script>
@stop
