@extends('layouts.app')

@section('content')
    <section class="module">
        <div class="container">
            <div class="heading">
                <h3>XE ĐIỆN NỔI BẬT</h3>
            </div>
            <ul class="category list">
                @for($i=0;$i<6;$i++)
                <li class="row">
                    <div class="col-md-4">
                        <div class="inner center">
                            <span class="real-price"><s>11.000.000đ</s></span>
                            <span class="price">10.600.000đ</span>
                            <a href="/xe-dien-vespas-dibao.html" title="" class="title">Xe điện ninja Dibao</a>
                            <p>Trải nghiệm công nghệ và sắc màu</p>
                            <a href="/xe-dien-vespas-dibao.html" title="Chi tiết" class="link-detail">Xem chi tiết <span class="fa fa-chevron-right"></span></a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <a href="/xe-dien-vespas-dibao.html" title="" class="thumb"><img src="/pictures/{{ ($i%2) ? 'xe-dien-ninja-dibao-cool@2x.png' : 'xe-dien-ninja-dibao-eco-t@2x.png'}}" alt="" /></a>
                    </div>
                </li>
                @endfor
            </ul>
            <article class="lorem-box">
                <p>Xe đạp điện Dibao thân thiện môi trường, không gây tiếng ồn, độ bền cao, đi êm nhẹ nhàng, đi nhanh mạnh mẽ, leo dốc hay đi ngược chiều gió không còn là vấn đề.</p>
                <p>Dibao là nhà phân phối độc quyền cho các hãng nổi tiếng tại Việt Nam như xe điện Yamaha, Giant, Bridgestone, Honda, Aima, Nijia…</p>

                <p>Dibao tự hào là nhà phân phối xe đạp điện được tin dùng nhất hiện nay:</p>

                <p>Công ty sản xuất và cung cấp xe 100% chất lượng tốt.</p>

                <p>Xe đạp điện có mức giá rẻ hơn các phương tiện khác gấp nhiều lần, đa dạng về giá cả, chi phí sử dụng ít.</p>

                <p>Xe đạp điện có trọng lượng nhỏ, mẫu mã đẹp, kiểu dáng thời trang, thuận tiện trong đi lại thích hợp với đối tượng học sinh, sinh viên.</p>

                <p>Thời gian sạc nhanh chóng, vận tốc xe đạt tối đa 40km/h, quãng đường xe chạy tối đa trong 1 lần sạc lến đến 50km.</p>

                <p>Hệ thống phân phối toàn quốc với hơn 400 đại lý, giao hàng tận nơi.</p>

                <p>Chính sách thanh toán, đổi trả thuận tiện.</p>

                <p>Muốn sở hữu mẫu xe đạp điện bền, đẹp thời trang, tiết kiệm tối đa điện năng hãy liên hệ đến 0913.197.866 để được tư vấn mua hàng.</p>
                <p>Chúng tôi luôn sẵn sàng phục vụ mọi quý khách hàng.</p>
            </article>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
