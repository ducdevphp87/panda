@extends('layouts.app')

@section('content')
    <section class="module" style="padding-top:40px">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-1">
                    <div class="box stores">
                        <h3>DANH SÁCH CỬA HÀNG TOÀN QUỐC</h3>
                        <p>Không ngừng cải thiện nâng cao chất lượng sản phẩm cũng như dịch vụ, chỉ sau 4 năm ra mắt, DIBAO hiện nay là một tên tuổi thuộc hàng “top” trong thị trường xe điện, với gần 200 cửa hàng phủ sóng trên toàn quốc</p>

                        <h2>Mua xe điện DIBAO ở đâu</h2>
                        <ul class="list tree-store" id="list_store">
                            <li><a href="javascript:;"><i class="fa fa-chevron-right"></i> Miền Bắc ( 25 )</a>
                                <ul>
                                    <li><a href="/daily/1/room" title=""><i class="fa fa-chevron-right"></i> Hà Nội</a>
                                        <ul>
                                            <li><a href="/daily/1/room" title=""><i class="fa fa-chevron-right"></i> Hà Nội</a></li>
                                            <li><a href="/daily/1/room" title=""><i class="fa fa-chevron-right"></i> Hải Phòng</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/daily/1/room" title=""><i class="fa fa-chevron-right"></i> Hải Phòng</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-chevron-right"></i> Miền Nam ( 25 )</a>
                                <ul>
                                    <li><a href="/daily/1/room" title=""><i class="fa fa-chevron-right"></i> Hồ Chí Minh</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;"><i class="fa fa-chevron-right"></i> Miền Trung ( 25 )</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-1 hidden-xs">
                    <div class="center">
                        <img src="/pictures/VietNamMap.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('layouts.inc_sp')
    @include('layouts.inc_cont')
@stop
@section('script')
<script type="text/javascript">
    $(function(){
        $('#list_store').find('li a').bind('click',function(e){
            if($(this).attr('href') == 'javascript:;'){
                e.preventDefault();
                $(this).parent().toggleClass('on')
            }

        })
    })

</script>
@stop
