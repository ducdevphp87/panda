<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::resource('/', 'ClientController\HomeController');
Route::get('salary', 'ClientController\PandaController@salary')->name('salary');

Route::post('/checkin', 'ClientController\HomeController@checkin');
Route::post('/checkout', 'ClientController\HomeController@checkout');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/panda', 'ClientController\PandaController');
    Route::get('employer', 'ClientController\PandaController@pandaEmp');
    Route::get('addUser', 'ClientController\PandaController@getAddUser')->name('getAddUser');
    Route::post('addUser', 'ClientController\PandaController@postAddUser');

    Route::get('sua_cong', 'ClientController\PandaController@suaCong');
    Route::post('sua_cong', 'ClientController\PandaController@suaCongPost');
    Route::post('/ngay_sua_cong', 'ClientController\PandaController@ngaysuaCongPost');

    Route::get('sua_cong_by_day', 'ClientController\PandaController@suaCongByDayGet');
    Route::post('sua_cong_by_day', 'ClientController\PandaController@suaCongByDayPost')->name('sua_cong_post');
    Route::get('/select_day', function(){
        return view('panda.select_day');
    });
    Route::resource('user', 'PhotoController');
    Route::get('salary_for_emp/{id}', 'ClientController\PandaController@salaryForEmp')->name('salary_for_emp');
    Route::post('salary_for_emp', 'ClientController\PandaController@salaryForEmpPost')->name('salary_for_emp');


});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
